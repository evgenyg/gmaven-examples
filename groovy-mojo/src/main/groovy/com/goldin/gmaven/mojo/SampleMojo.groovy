package com.goldin.gmaven.mojo

import org.apache.maven.execution.MavenSession
import org.apache.maven.project.MavenProject
import org.codehaus.gmaven.mojo.GroovyMojo
import org.jfrog.maven.annomojo.annotations.MojoGoal
import org.jfrog.maven.annomojo.annotations.MojoParameter
import org.jfrog.maven.annomojo.annotations.MojoPhase

/**
 * Sample Groovy Maven plugin
 */
@MojoGoal  ( 'run' )
@MojoPhase ( 'validate' )
public class SampleMojo extends GroovyMojo
{
    @MojoParameter
    public String runIf

    @MojoParameter ( required = true )
    public String name

    @MojoParameter
    public String propName

    @MojoParameter ( required = false, defaultValue = 'default-value' )
    public String value
    
    @MojoParameter ( required = false )
    public String title = 'default-title'

    @MojoParameter ( expression = '${project}', required = true )
    public MavenProject project

    @MojoParameter ( expression = '${session}', required = true )
    public MavenSession session

    @MojoParameter ( expression = '${project.basedir}', required = true )
    public File basedir


    @Override
    public void execute ()
    {
        if ( runIf )
        {
            Binding binding = new Binding([ project: project, session : session ])
            Object  result  = new GroovyShell( binding ).evaluate( runIf )
            boolean isRun   = Boolean.valueOf( String.valueOf( result ))

            if ( isRun )
            {
                log.info( "[$result]: plugin is running" )
            }
            else
            {
                log.info( "[$result]: plugin is not running" )
                return
            }
        }

        if ( propName )
        {
            session.executionProperties[ propName ] = System.currentTimeMillis() as String
            log.info( "Property [$propName] set to [${ session.userProperties[ propName ] }]" )
        }

        log.info( "[$name][$value][$title]" )
        log.info( "[$project][$session][$basedir]" )
    }
}
