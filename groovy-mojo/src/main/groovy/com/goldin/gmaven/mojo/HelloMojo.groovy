package com.goldin.gmaven.mojo

import org.codehaus.gmaven.mojo.GroovyMojo
import org.jfrog.maven.annomojo.annotations.MojoGoal
import org.jfrog.maven.annomojo.annotations.MojoPhase
import org.jfrog.maven.annomojo.annotations.MojoParameter

@MojoGoal  ( 'hello' )
@MojoPhase ( 'validate' )
class HelloMojo extends GroovyMojo
{
    @MojoParameter
    public String name


    @Override
    void execute ()
    {
        log.println "Hello, $name"
    }
}
