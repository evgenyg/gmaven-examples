project {

    modelVersion '4.0.0'
    groupId      'com.goldin.gmaven'
    artifactId   'main'
    version      '0.1-SNAPSHOT'
    packaging    'pom'
    name         '[${project.groupId}:${project.artifactId}:${project.version}]'

    modules ( 'groovy-classes',
              'groovy-mojo',
              'run-groovy-code',
              'run-groovy-script',
              'run-groovy-class',
              'run-groovy-mojo' )

    properties {
        'spock-version'    '0.5-groovy-1.8'
        'annomojo-version' '1.4.0'
        'groovy-version'   '1.8.0'
        'hamcrest-version' '1.3.RC2'
        'gmaven-version'   '1.3'
        'maven-version'    '3.0.3'
    }


    build {
        plugins {
            plugin {
                artifactId 'maven-enforcer-plugin'
                version    '1.0'
                executions {
                    execution {
                        id    ( 'enforce-maven' )
                        phase ( 'validate' )
                        goals ( 'display-info', 'enforce' )
                        configuration {
                            rules { requireMavenVersion { version ( '3.0' ) }}
                        }
                    }
                }
            }
        }
    }
}
