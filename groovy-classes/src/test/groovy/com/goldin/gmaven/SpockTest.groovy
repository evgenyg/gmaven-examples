package com.goldin.gmaven

import org.junit.Test
import spock.lang.Specification
import static org.hamcrest.CoreMatchers.equalTo
import static org.hamcrest.MatcherAssert.assertThat


/**
 * {@link Sample} Spock test
 */
class SpockTest extends Specification
{
    @Test
    void "Should summarize two int's"()
    {
        when:
        def a = 37
        def b = 63

        then:
        new Sample().sum( a, b ) == 100
        assertThat( new Sample().sum( a, b ), equalTo( 100 ))
    }


    @Test
    void "Should multiply two int's"()
    {
        def a = 37
        def b = 63

        expect:
        new Sample().mult( a, b ) == 2331
        assertThat( new Sample().mult( a, b ), equalTo( 2331 ))
        assertThat(( 34 * 56 ),                equalTo( 1904 ))
        // assertThat( new Sample().mult( a, b ), equalTo( 2332 )) Uncomment to fail the test
    }
}
