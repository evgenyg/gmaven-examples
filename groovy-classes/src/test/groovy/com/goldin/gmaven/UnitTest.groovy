package com.goldin.gmaven

import org.junit.Test

/**
 * {@link Sample} unit test
 */
class UnitTest
{
    @Test
    void shouldSummarize()
    {
        assert 100 == new Sample().sum( 37, 63 )
    }


    @Test
    void shouldMultiply()
    {
        assert 2331 == new Sample().mult( 37, 63 )
    }
}
