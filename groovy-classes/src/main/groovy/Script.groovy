def find   = { command, prefix -> command.execute().text.
                                  readLines().find{ it.startsWith( prefix ) }.
                                  replace( prefix, '' ).trim() }
                                
session.executionProperties += [ coordinates : "[$project.groupId][$project.artifactId][$project.version]".toString(),
                                 buildDate   : session.startTime.toString(),
                                 os          : System.getProperty( 'os.name' ),
                                 repo        : find( 'git remote -v', 'origin' ),
                                 branch      : find( 'git status',    '# On branch' ),
                                 commit      : find( 'git log',       'commit' ),
                                 commitDate  : find( 'git log',       'Date:' ) ]
