package com.goldin.gmaven

import org.apache.maven.execution.MavenSession
import org.apache.maven.project.MavenProject

class Sample
{

    int sum  ( int a, int b ) { a + b }
    int mult ( int a, int b ) { a * b }


    static void main ( String ... args )
    {
        println "AAAAAAAAAAAAA"
    }


    void setProperties ( MavenProject project, MavenSession session )
    {
        session.executionProperties += [ coordinates : "[$project.groupId][$project.artifactId][$project.version]".toString(),
                                         buildDate   : session.startTime.toString(),
                                         os          : System.getProperty( 'os.name' ),
                                         repo        : find( 'git remote -v', 'origin' ),
                                         branch      : find( 'git status',    '# On branch' ),
                                         commit      : find( 'git log',       'commit' ),
                                         commitDate  : find( 'git log',       'Date:' ) ]
    }


    String find( String command, String prefix )
    {
        command.execute().text.
                readLines().find{ it.startsWith( prefix ) }.
                replace( prefix, '' ).trim()
    }
}
