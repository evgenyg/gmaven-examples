project {
    modelVersion '4.0.0'
    groupId      'com.goldin.gmaven'
    artifactId   'groovy-classes'
    version      '0.1-SNAPSHOT'
    name         '[${project.groupId}:${project.artifactId}:${project.version}]'

    parent ( 'com.goldin.gmaven:main:0.1-SNAPSHOT' )

    dependencies {
        dependency( 'org.apache.maven:maven-core:${maven-version}:compile' )
        dependency( 'org.codehaus.groovy:groovy-all:${groovy-version}:compile' )
        dependency( 'org.hamcrest:hamcrest-core:${hamcrest-version}:test' )
        dependency( 'org.spockframework:spock-core:${spock-version}:test' )
    }

    build {
        plugins {
            plugin {
                groupId    'org.codehaus.gmaven'
                artifactId 'gmaven-plugin'
                version    '${gmaven-version}'
                executions {
                    execution {
                        id     'set-manifest-props-script'
                        phase  'prepare-package'
                        goals( 'execute' )
                        configuration { source '${project.basedir}/src/main/groovy/Script.groovy' }
                    }
                    execution {
                        id      'compile-groovy-classes'
                        phase   'compile'
                        goals ( 'compile' )
                    }
                    execution {
                        id     'compile-groovy-tests'
                        phase  'test-compile'
                        goals( 'testCompile' )
                    }
                }
                dependencies {
                    dependency( 'org.codehaus.groovy:groovy-all:${groovy-version}' )
                    dependency( 'org.codehaus.gmaven.runtime:gmaven-runtime-1.7:${gmaven-version}' ) {
                        exclusions{ exclusion( 'org.codehaus.groovy:groovy-all' ) }
                    }
                }
                configuration {
                    providerSelection ( '1.7' )
                    source()
                }
            }

            plugin {
                artifactId 'maven-jar-plugin'
                version '2.3.1'
                configuration {
                    archive {
                        manifestEntries {
                            Coordinates  ( '${coordinates}' )
                            OS           ( '${os}'          )
                            BuildDate    ( '${buildDate}'   )
                            Repo         ( '${repo}'        )
                            Branch       ( '${branch}'      )
                            Commit       ( '${commit}'      )
                            CommitDate   ( '${commitDate}'  )
                        }
                    }
                }
            }
        }
    }
}
