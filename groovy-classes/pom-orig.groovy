project {
    modelVersion '4.0.0'
    parent {
        artifactId 'main'
        groupId 'com.goldin.gmaven'
        version '0.1-SNAPSHOT'
    }
    groupId 'com.goldin.gmaven'
    artifactId 'groovy-classes'
    version '0.1-SNAPSHOT'
    name '[${project.groupId}:${project.artifactId}:${project.version}]'
    dependencies {
        dependency {
            groupId 'org.apache.maven'
            artifactId 'maven-core'
            version '${maven-version}'
            scope 'compile'
        }
        dependency {
            groupId 'org.codehaus.groovy'
            artifactId 'groovy-all'
            version '${groovy-version}'
            scope 'compile'
        }
        dependency {
            groupId 'org.hamcrest'
            artifactId 'hamcrest-core'
            version '${hamcrest-version}'
            scope 'test'
        }
        dependency {
            groupId 'org.spockframework'
            artifactId 'spock-core'
            version '${spock-version}'
            scope 'test'
            exclusions {
                exclusion {
                    artifactId 'groovy-all'
                    groupId 'org.codehaus.groovy'
                }
                exclusion {
                    artifactId 'hamcrest-core'
                    groupId 'org.hamcrest'
                }
            }
        }
    }
    build {
        plugins {
            plugin {
                groupId 'org.codehaus.gmaven'
                artifactId 'gmaven-plugin'
                version '${gmaven-version}'
                executions {
                    execution {
                        id 'set-manifest-props'
                        phase 'prepare-package'
                        goals {
                            goal 'execute'
                        }
                        configuration {
                            source '''def find   = { command, prefix -> command.execute().text.readLines().find{ it.startsWith( prefix ) }.replace( prefix, '' ).trim() }
                                def gitDir = new File( '.git' )

                                assert gitDir.isDirectory(), "[$gitDir.canonicalPath] is not found"
                                session.executionProperties += [ coordinates : "[$project.groupId][$project.artifactId][$project.version]".toString(),
                                                                 buildDate   : session.startTime.toString(),
                                                                 os          : System.getProperty( 'os.name' ),
                                                                 repo        : find( 'git remote -v', 'origin' ),
                                                                 branch      : find( 'git status',    '# On branch' ),
                                                                 commit      : find( 'git log',       'commit' ),
                                                                 commitDate  : find( 'git log',       'Date:' ) ]'''
                        }
                    }
                    execution {
                        id 'set-manifest-props-script'
                        phase 'prepare-package'
                        goals {
                            goal 'execute'
                        }
                        configuration {
                            source '${project.basedir}/src/main/groovy/Script.groovy'
                        }
                    }
                    execution {
                        id 'set-manifest-props-class'
                        phase 'prepare-package'
                        goals {
                            goal 'execute'
                        }
                        configuration {
                            scriptpath {
                                element '${project.basedir}/src/main/groovy'
                            }
                            source 'new com.goldin.gmaven.Sample().setProperties( project, session )'
                        }
                    }
                    execution {
                        id 'compile-groovy-classes'
                        phase 'compile'
                        goals {
                            goal 'compile'
                        }
                    }
                    execution {
                        id 'compile-groovy-tests'
                        phase 'test-compile'
                        goals {
                            goal 'testCompile'
                        }
                    }
                }
                dependencies {
                    dependency {
                        groupId 'org.codehaus.gmaven.runtime'
                        artifactId 'gmaven-runtime-1.7'
                        version '${gmaven-version}'
                        exclusions {
                            exclusion {
                                artifactId 'groovy-all'
                                groupId 'org.codehaus.groovy'
                            }
                        }
                    }
                    dependency {
                        groupId 'org.codehaus.groovy'
                        artifactId 'groovy-all'
                        version '${groovy-version}'
                    }
                }
                configuration {
                    providerSelection '1.7'
                    source
                }
            }
            plugin {
                artifactId 'maven-jar-plugin'
                version '2.3.1'
                configuration {
                    archive {
                        manifestEntries {
                            Coordinates '${coordinates}'
                            OS '${os}'
                            BuildDate '${buildDate}'
                            Repo '${repo}'
                            Branch '${branch}'
                            Commit '${commit}'
                            CommitDate '${commitDate}'
                        }
                    }
                }
            }
        }
    }
}
