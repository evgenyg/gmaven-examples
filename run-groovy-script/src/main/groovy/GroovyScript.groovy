import static org.hamcrest.CoreMatchers.*
import static org.hamcrest.MatcherAssert.*
import static org.hamcrest.core.AllOf.*
import static org.hamcrest.core.IsNot.*
import static org.hamcrest.core.IsNull.*


if (( 3 < 5 ) && ( 5 > 3 ))
{
    println "Time is [[[[[[" + new Date() + "]]]]]]"
}

assert 34 * 56 == 1904
assertThat(( 34 * 56 ), equalTo( 1904 ))
assertThat(( 34 * 56 ), allOf( equalTo( 1904 ),
                               not( nullValue()),
                               notNullValue()))
