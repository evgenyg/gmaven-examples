package com.goldin.gmaven

import org.apache.maven.execution.MavenSession
import org.apache.maven.project.MavenProject


/**
 * Simple helper class
 */
class GroovyHelper
{
    GroovyHelper ()
    {
    }

    void doSomething( MavenProject project, MavenSession session )
    {
        println "[${ project.model.artifactId }][${ session.goals }]"
    }
}
